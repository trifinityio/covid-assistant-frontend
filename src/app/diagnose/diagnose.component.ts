import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diagnose',
  templateUrl: './diagnose.component.html',
  styleUrls: ['./diagnose.component.scss']
})
export class DiagnoseComponent {
  public step = 1;
  public allSteps = 11;
  public score = 0;
  public questionnaire = {
    temperature: '36.6',
    fatigued: '0',
    headache: '0',
    bodyache: '0',
    cough: '0',
    runnyNose: '0',
    sneeze: '0',
    soreThroat: '0',
    diarrhoea: '0',
    respiratory: '0',
    contact: '0',
  };

  constructor() { }

  public nextStep() {
    this.step++;
    if (this.step > this.allSteps) {
      this.calculateScore();
    }
  }

  private calculateScore() {
    const norm = this.normalizeQuestionnaire();
    let score = 0;
    if (norm.temperature > 36.9 && norm.temperature < 38) {
      score += 1;
    } else if (norm.temperature > 38) {
      score += 3;
    }

    if (norm.fatigued) {
      score += 2;
    }

    if (norm.headache) {
      score += 2;
    }

    if (norm.bodyache) {
      score += 2;
    }

    if (norm.headache) {
      score += 2;
    }

    if (norm.cough === 1) {
      score += 3;
    } else if (norm.cough === 2) {
      score += 1.5;
    }

    if (norm.runnyNose) {
      score += 2;
    }

    if (norm.sneeze) {
      score -= 1;
    } else {
      score += 1;
    }

    if (norm.soreThroat) {
      score += 2;
    }

    if (norm.diarrhoea) {
      score += 1; // tu jeszcze trzeba uwzględnić wiek
    }

    if (norm.respiratory) {
      score += 2;
    }

    if (norm.contact) {
      score += 2;
    }

    this.score = Math.round((score / 27) * 100);
  }

  private normalizeQuestionnaire() {
    return {
      temperature: +this.questionnaire.temperature,
      fatigued: +this.questionnaire.temperature,
      headache: +this.questionnaire.headache,
      bodyache: +this.questionnaire.bodyache,
      cough: +this.questionnaire.cough,
      runnyNose: +this.questionnaire.runnyNose,
      sneeze: +this.questionnaire.sneeze,
      soreThroat: +this.questionnaire.soreThroat,
      diarrhoea: +this.questionnaire.diarrhoea,
      respiratory: +this.questionnaire.respiratory,
      contact: +this.questionnaire.respiratory,
    };
  }
}
