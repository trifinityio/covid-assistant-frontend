import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
  public helpElements = [
    {
      title: 'HELP.Q1',
      description: 'HELP.A1',
    },
    {
      title: 'HELP.Q2',
      description: 'HELP.A2',
    },
    {
      title: 'HELP.Q3',
      description: 'HELP.A3',
    },
    {
      title: 'HELP.Q4',
      description: 'HELP.A4',
      type: 'multiple',
      multiple: [
        {
          title: 'HELP.A4.Q1',
          description: 'HELP.A4.A1'
        },
        {
          title: 'HELP.A4.Q2',
          description: 'HELP.A4.A2'
        },
        {
          title: 'HELP.A4.Q3',
          description: 'HELP.A4.A3'
        },
        {
          title: 'HELP.A4.Q4',
          description: 'HELP.A4.A4'
        },
        {
          title: 'HELP.A4.Q5',
          description: 'HELP.A4.A4'
        },
        {
          title: 'HELP.A4.Q6',
          description: 'HELP.A4.A6'
        },
        {
          title: 'HELP.A4.Q7',
          description: 'HELP.A4.A7'
        },
        {
          title: 'HELP.A4.Q8',
          description: 'HELP.A4.A8'
        },
      ],
    },
    {
      title: 'HELP.Q5',
      description: 'HELP.A5',
    },
    {
      title: 'HELP.Q6',
      description: 'HELP.A6',
    },
    {
      title: 'HELP.Q7',
      description: 'HELP.A7',
    },
    {
      title: 'HELP.Q8',
      description: 'HELP.A8',
    },
    {
      title: 'HELP.Q9',
      description: 'HELP.A9',
    },
    {
      title: 'HELP.Q10',
      description: 'HELP.A10',
    },
    {
      title: 'HELP.Q11',
      description: 'HELP.A11',
    },
    {
      title: 'HELP.Q12',
      description: 'HELP.A12',
    },
    {
      title: 'HELP.Q13',
      description: 'HELP.A13',
    },
    {
      title: 'HELP.Q14',
      description: 'HELP.A14',
    },
    {
      title: 'HELP.Q15',
      description: 'HELP.A15',
    },
    {
      title: 'HELP.Q16',
      description: 'HELP.A16',
    },
  ];

  public recommendations = [
    {
      title: 'RECOMMENDATIONS.Q1',
      description: 'RECOMMENDATIONS.A1',
    },
    {
      title: 'RECOMMENDATIONS.Q2',
      description: 'RECOMMENDATIONS.A2',
    },
    {
      title: 'RECOMMENDATIONS.Q3',
      description: 'RECOMMENDATIONS.A3',
    },
    {
      title: 'RECOMMENDATIONS.Q4',
      description: 'RECOMMENDATIONS.A4',
    },
    {
      title: 'RECOMMENDATIONS.Q5',
      description: 'RECOMMENDATIONS.A5',
    },
    {
      title: 'RECOMMENDATIONS.Q6',
      description: 'RECOMMENDATIONS.A6',
    },
    {
      title: 'RECOMMENDATIONS.Q7',
      description: 'RECOMMENDATIONS.A7',
    },
    {
      title: 'RECOMMENDATIONS.Q8',
      description: 'RECOMMENDATIONS.A8',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
