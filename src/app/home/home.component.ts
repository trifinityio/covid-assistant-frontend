import { Component, OnInit } from "@angular/core";
import { faBookDead } from "@fortawesome/free-solid-svg-icons/faBookDead";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons/faExclamationTriangle";
import { faStethoscope } from "@fortawesome/free-solid-svg-icons/faStethoscope";
import { faSortAmountUpAlt } from "@fortawesome/free-solid-svg-icons/faSortAmountUpAlt";
import { faSortAmountDown } from "@fortawesome/free-solid-svg-icons/faSortAmountDown";

import { ApiService, CountryStats } from "../../services/api.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public faBookDead = faBookDead;
  public faExclamationTriangle = faExclamationTriangle;
  public faStethoscope = faStethoscope;
  public faSortAmountUpAlt = faSortAmountUpAlt;
  public faSortAmountDown = faSortAmountDown;

  public summary = {
    confirmed: 0,
    deaths: 0,
    recovered: 0
  };
  public countries: CountryStats[] = [];
  public sort = {
    column: 'cases',
    order: 'desc',
  };

  private notFilteredCountries: CountryStats[] = [];

  constructor(private _api: ApiService) {}

  ngOnInit(): void {
    this._api.getAll().subscribe(data => {
      this.summary.confirmed = data.cases;
      this.summary.deaths = data.deaths;
      this.summary.recovered = data.recovered;
    });

    this._api.getCountries().subscribe(data => {
      this.countries = data;
      this.notFilteredCountries = data.slice();
    });
  }

  public newCase(deaths: number) {
    if (deaths > 0) {
      return '+' + deaths;
    }

    return '';
  }

  public sortBy(columnName: string) {
    if (this.sort.column !== columnName) {
      this.sort.order = 'desc';
      this.sort.column = columnName;
    } else {
      if (this.sort.order === 'desc') {
        this.sort.order = 'asc';
      } else {
        this.sort.order = 'desc';
      }
    }

    this.applySort();
  }

  public handleSearch(text) {
    const searchBy = text.toLowerCase().trim();
    this.countries = this.notFilteredCountries.filter(i => i.country.toLowerCase().includes(searchBy));
    this.applySort();
  }

  public getSortIcon() {
    if (this.sort.order === 'desc') {
      return faSortAmountDown;
    } else {
      return faSortAmountUpAlt;
    }
  }

  private applySort() {
    const copy = this.countries.slice();
    if (this.sort.order === 'desc') {
      copy.sort((a, b) => b[this.sort.column] - a[this.sort.column]);
    } else {
      copy.sort((a, b) => a[this.sort.column] - b[this.sort.column]);
    }
    this.countries = copy;
  }
}
