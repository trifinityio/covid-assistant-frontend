import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DiagnoseComponent } from './diagnose/diagnose.component';
import { ThanksComponent } from './thanks/thanks.component';
import { GooglePlacesComponent } from './google-places/google-places.component';
import { HelpComponent } from './help/help.component';
import { MapComponent } from './map/map.component';
import { RecommendationsComponent } from './recommendations/recommendations.component';

const routes: Routes = [
  { path: '', component: DiagnoseComponent },
  { path: ':lang', children: [
    {path: 'diagnose', component: DiagnoseComponent},
    {path: 'thanks', component: ThanksComponent},
    {path: 'help', component: HelpComponent},
    {path: 'stats', component: HomeComponent},
    {path: 'map', component: MapComponent},
    {path: 'recommendations', component: RecommendationsComponent},
    {path: 'google-places', component: GooglePlacesComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
