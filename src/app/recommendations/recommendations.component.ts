import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recommendations',
  templateUrl: './recommendations.component.html',
  styleUrls: ['./recommendations.component.scss']
})
export class RecommendationsComponent implements OnInit {
  public recommendations = [
    {
      title: 'RECOMMENDATIONS.Q1',
      description: 'RECOMMENDATIONS.A1',
    },
    {
      title: 'RECOMMENDATIONS.Q2',
      description: 'RECOMMENDATIONS.A2',
    },
    {
      title: 'RECOMMENDATIONS.Q3',
      description: 'RECOMMENDATIONS.A3',
    },
    {
      title: 'RECOMMENDATIONS.Q4',
      description: 'RECOMMENDATIONS.A4',
    },
    {
      title: 'RECOMMENDATIONS.Q5',
      description: 'RECOMMENDATIONS.A5',
    },
    {
      title: 'RECOMMENDATIONS.Q6',
      description: 'RECOMMENDATIONS.A6',
    },
    {
      title: 'RECOMMENDATIONS.Q7',
      description: 'RECOMMENDATIONS.A7',
    },
    {
      title: 'RECOMMENDATIONS.Q8',
      description: 'RECOMMENDATIONS.A8',
    },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
