import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { AnalyticsService } from './shared/analytics.service';
import { DiagnoseComponent } from './diagnose/diagnose.component';
import { HomeComponent } from './home/home.component';
import { ThanksComponent } from './thanks/thanks.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GooglePlacesComponent } from './google-places/google-places.component';
import { FormsModule } from '@angular/forms';
import { HelpComponent } from './help/help.component';
import { MapComponent } from './map/map.component';
import { RecommendationsComponent } from './recommendations/recommendations.component';
import { DiagnoseRecommendationComponent } from './diagnose-recommendation/diagnose-recommendation.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
    DiagnoseComponent,
    HomeComponent,
    ThanksComponent,
    GooglePlacesComponent,
    HelpComponent,
    MapComponent,
    RecommendationsComponent,
    DiagnoseRecommendationComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [AnalyticsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
