import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { faFacebookF } from "@fortawesome/free-brands-svg-icons/faFacebookF";
import { faTwitter } from "@fortawesome/free-brands-svg-icons/faTwitter";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons/faLinkedin";
import { faStethoscope } from '@fortawesome/free-solid-svg-icons/faStethoscope';
import { faUserMd } from '@fortawesome/free-solid-svg-icons/faUserMd';
import { faMedkit } from '@fortawesome/free-solid-svg-icons/faMedkit';
import { faMap } from '@fortawesome/free-solid-svg-icons/faMap';
import { faShareSquare } from '@fortawesome/free-solid-svg-icons/faShareSquare';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons/faInfoCircle';

import { AnalyticsService } from './shared/analytics.service';
import { NavigationStart, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

const languages = ['en', 'pl'];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public faFacebookF = faFacebookF;
  public faTwitter = faTwitter;
  public faLinkedin = faLinkedin;
  public faStethoscope = faStethoscope;
  public faUserMd = faUserMd;
  public faMedkit = faMedkit;
  public faMap = faMap;
  public faShare = faShareSquare;
  public faInfoCircle = faInfoCircle;
  public promptEvent;
  public appInstaled = false;

  constructor(
    private analyticsService: AnalyticsService,
    public router: Router,
    private title: Title,
    public translate: TranslateService,
    private metaService: Meta
  ) {
    this.initializeApp();

    translate.addLangs(languages);
    translate.setDefaultLang('en');

    const lang = localStorage.getItem('lang');
    if (lang) {
      translate.use(lang);
    } else {
      const browserLang = translate.getBrowserLang();
      const langToSet = browserLang.match(/en|pl/) ? browserLang : 'en';
      translate.use(langToSet);
      localStorage.setItem('lang', langToSet);
    }
  }

  public installPwa(): void {
    this.promptEvent.prompt();
  }

  private initializeApp() {
    window.addEventListener('beforeinstallprompt', event => {
      this.promptEvent = event;
    });
    // window.addEventListener('appinstalled', (evt) => {
    //   this.appInstaled = true;
    // });
    // Start track passing Tracker Id
    this.analyticsService.startTrackerWithId('UA-161485743-1');
    this.router.events
    .subscribe(event => {
      // observe router and when it start navigation it will track the view
      if (event instanceof NavigationStart) {
        let title = this.title.getTitle();
        // get title if it was sent on state
        if (this.router.getCurrentNavigation().extras.state) {
          title = this.router.getCurrentNavigation().extras.state.title;
        }
        // pass url and page title
        this.analyticsService.trackView(event.url, title);

        const currentLang = this.translate.currentLang;
        const lang = event.url.split('/')[1];
        if (languages.includes(lang) && lang !== currentLang) {
          this.translate.use(lang);
          localStorage.setItem('lang', lang);
        }
      }
    });
  }

  ngOnInit() {
    this.metaService.addTags([
      {
        name: 'description',
        content: 'Covid Assistant to Twój wirtualny asystent w walce z koronawirusem. Przeprowadzisz w nim darmową autodiagnozę a także sprawdzisz aktualną sytuację w okolicy.'
      },
    ]);
  }
}
