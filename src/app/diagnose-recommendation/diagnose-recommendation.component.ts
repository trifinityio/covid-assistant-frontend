import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-diagnose-recommendation',
  templateUrl: './diagnose-recommendation.component.html',
  styleUrls: ['./diagnose-recommendation.component.scss']
})
export class DiagnoseRecommendationComponent {
  @Input() public result;

  constructor() { }

  public getText() {
    if (this.result <= 50) {
      return 'diagnoseRecommendation.<50';
    } else if (this.result > 50 && this.result < 85) {
      return 'diagnoseRecommendation.>50';
    } else if (this.result >= 85) {
      return 'diagnoseRecommendation.<85';
    }
  }
}
