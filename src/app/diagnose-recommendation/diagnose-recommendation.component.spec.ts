import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnoseRecommendationComponent } from './diagnose-recommendation.component';

describe('DiagnoseRecommendationComponent', () => {
  let component: DiagnoseRecommendationComponent;
  let fixture: ComponentFixture<DiagnoseRecommendationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnoseRecommendationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnoseRecommendationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
