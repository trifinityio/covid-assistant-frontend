import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export interface AllResponse {
  cases: number;
  deaths: number;
  recovered: number;
  updated: number;
}

export interface CountryStats {
  country: string;
  cases: number;
  todayCases: number;
  deaths: number;
  todayDeaths: number;
  recovered: number;
  active: number;
  critical: number;
  casesPerOneMillion: number;
}

@Injectable({
  providedIn: "root"
})
export class ApiService {
  constructor(private http: HttpClient) {}

  public getAll(): Observable<AllResponse> {
    return this.http.get<AllResponse>("https://corona.lmao.ninja/all");
  }

  public getCountries(): Observable<CountryStats[]> {
    return this.http.get<CountryStats[]>("https://corona.lmao.ninja/countries");
  }
}
